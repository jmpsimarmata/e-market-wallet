from api.models import TopUpBank, TopUpBuktiPembayaran
from apscheduler.schedulers.background import BackgroundScheduler
from api.models import EWallet
import datetime
from django.core.cache import cache

def verify_top_up():
     print('verifying top up...')
     top_up_bank_objects = TopUpBank.objects.all()
     top_up_bukti_objects = TopUpBuktiPembayaran.objects.all()

     for with_bank in top_up_bank_objects:
          if(not with_bank.is_verified):
               now  = datetime.datetime.now()
               time_verified = with_bank.created_at + datetime.timedelta(minutes=5)

               if time_verified.strftime('%H:%M:%S') < now.strftime('%H:%M:%S'):
                    print('{} < {}'.format(time_verified.strftime('%H:%M:%S'), now.strftime('%H:%M:%S')))
                    with_bank.is_verified = True
                    with_bank.save()

                    ewallet = EWallet.objects.get(username=with_bank.username)
                    ewallet.saldo += int(with_bank.nominal)
                    ewallet.save()

                    cache.set(ewallet.username, ewallet, timeout=(60*5))

     for with_bukti in top_up_bukti_objects:
          if(not with_bukti.is_verified):
               now  = datetime.datetime.now()
               time_verified = with_bukti.created_at + datetime.timedelta(minutes=5)

               if time_verified.strftime('%H:%M:%S') < now.strftime('%H:%M:%S'):
                    print('{} < {}'.format(time_verified.strftime('%H:%M:%S'), now.strftime('%H:%M:%S')))
                    with_bukti.is_verified = True
                    with_bukti.save()

                    ewallet = EWallet.objects.get(username=with_bukti.username)
                    ewallet.saldo += int(with_bukti.nominal)
                    ewallet.save()

                    cache.set(ewallet.username, ewallet, timeout=(60*5))

def start():
     scheduler = BackgroundScheduler()
     scheduler.add_job(verify_top_up, 'interval', minutes = 1, id='verify_top_up', replace_existing=True)
     scheduler.start()