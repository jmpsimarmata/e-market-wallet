from django.db import models
from cloudinary.models import CloudinaryField
# import datetime
class EWallet(models.Model):
     username = models.CharField(max_length=255, primary_key=True)
     saldo = models.IntegerField()

     def __str__(self) -> str:
         return self.username + ' - ' + str(self.saldo)

class TopUpBank(models.Model):
     username = models.CharField(max_length=255)
     nominal = models.IntegerField()
     bank = models.CharField(max_length=255)
     nama_kartu = models.CharField(max_length=255)
     nomor_kartu = models.CharField(max_length=255)
     expired_date = models.CharField(max_length=50)
     card_verification_code = models.CharField(max_length=3)
     is_verified = models.BooleanField(default=False)
     created_at = models.DateTimeField(blank=True, auto_now_add=True)

class TopUpBuktiPembayaran(models.Model):
     username = models.CharField(max_length=255)
     nominal = models.IntegerField()
     image = CloudinaryField('image')
     is_verified = models.BooleanField(default=False)
     created_at = models.DateTimeField(blank=True, auto_now_add=True)

class PembayaranDenganEWallet(models.Model):
     username = models.CharField(max_length=255)
     nominal =  models.IntegerField()
     deskripsi = models.CharField(max_length=255, null=True)



