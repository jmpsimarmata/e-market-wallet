from django.contrib import admin
from api.models import EWallet, PembayaranDenganEWallet, TopUpBuktiPembayaran, TopUpBank
# Register your models here.

class TopUpBankAdmin(admin.ModelAdmin):
    list_display = ('username', 'nominal', 'is_verified', 'created_at')

class TopUpBuktiPembayaranAdmin(admin.ModelAdmin):
    list_display = ('username', 'nominal', 'is_verified', 'created_at')

admin.site.register(EWallet)
admin.site.register(TopUpBuktiPembayaran, TopUpBuktiPembayaranAdmin)
admin.site.register(TopUpBank, TopUpBankAdmin)
admin.site.register(PembayaranDenganEWallet)