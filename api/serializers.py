from rest_framework import serializers
from api.models import PembayaranDenganEWallet, TopUpBank, TopUpBuktiPembayaran, EWallet

class EWalletSerializer(serializers.ModelSerializer):
     class Meta:
          model = EWallet
          fields = '__all__'

class TopUpBankSerializer(serializers.ModelSerializer):
     e_wallet = serializers.SerializerMethodField('get_ewallet')
     class Meta:
          model = TopUpBank
          fields = (
               'id',
               'username',
               'nominal',
               'bank',
               'nama_kartu',
               'nomor_kartu',
               'expired_date',
               'card_verification_code',
               'e_wallet'
          )

     def get_ewallet(self, data):
          return EWalletSerializer(
               EWallet.objects.get(pk=data.username)
          ).data


class TopUpBuktiPembayaranSerializer(serializers.ModelSerializer):
     e_wallet = serializers.SerializerMethodField('get_ewallet')
     class Meta:
          model = TopUpBuktiPembayaran
          fields = (
               'id',
               'username',
               'nominal',
               'image',
               'e_wallet'
          )
     
     def get_ewallet(self, data):
          return EWalletSerializer(
               EWallet.objects.get(pk=data.username)
          ).data

class PembayaranDenganEWalletSerializer(serializers.ModelSerializer):
     e_wallet = serializers.SerializerMethodField('get_ewallet')
     class Meta:
          model = PembayaranDenganEWallet
          fields = (
               'id',
               'username',
               'nominal',
               'deskripsi',
               'e_wallet'
          )
     
     def get_ewallet(self, data):
          return EWalletSerializer(
               EWallet.objects.get(pk=data.username)
          ).data