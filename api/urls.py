from django.urls import path
from api.views import get_ewallet, top_up_with_upload_bukti_pembayaran, top_up_with_bank, pay_with_ewallet

app_name = 'api'
urlpatterns = [
     path('e-wallet/<str:username>/', get_ewallet, name='get_ewallet'),
     path('e-wallet/top-up/bukti-pembayaran', top_up_with_upload_bukti_pembayaran, name='top_up_with_upload_bukti_pembayaran'),
     path('e-wallet/top-up/bank', top_up_with_bank, name='top_up_with_bank'),
     path('e-wallet/bayar', pay_with_ewallet, name='pay_with_ewallet')
]