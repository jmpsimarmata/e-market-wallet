from rest_framework.decorators import api_view
from django.views.decorators.http import require_http_methods
from rest_framework.response import Response
from rest_framework import status

from api.models import EWallet

from api.serializers import PembayaranDenganEWalletSerializer, TopUpBankSerializer, TopUpBuktiPembayaranSerializer, EWalletSerializer

from django.conf import settings
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.core.cache import cache

import requests
import json

CACHE_TTL  = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)

def verify_token(token):
     url = 'http://tk.oauth.getoboru.xyz/token/resource'
     response = requests.get(
          url,
          headers={'Authorization' : '{}'.format(token)},
          data={}
     )
     if response.status_code == 200:
          return True
     return False

def send_log(type, message):
     url = 'http://tk.logger.getoboru.xyz/{}'.format(type)
     data = {'message': message, 'app': 'E-Wallet Service'}
     headers = {'Content-type': 'application/json'}
     r = requests.post(url, data=json.dumps(data), headers=headers)

@require_http_methods(["POST"])
@api_view(['POST'])
def top_up_with_upload_bukti_pembayaran(request):
     if not 'HTTP_AUTHORIZATION' in request.META:
          send_log('error', 'Unauthorized user is accessing endpoint top up e-wallet with upload bukti pembayaran with no token')
          return Response(
               {
                    'error': 'No token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

     is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
     if not is_token_verified:
          send_log('error', 'Unauthorized user is accessing endpoint top up e-wallet with upload bukti pembayaran with invalid token')
          return Response(
               {
                    'error': 'Invalid token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )
     data = request.data
     serializer = TopUpBuktiPembayaranSerializer(data=data)
     if serializer.is_valid():
          serializer.save()
          send_log('info', 'Authorized user has Top up E-Wallet with upload bukti pembayaran and the transaction is being processed')
          return Response(
               {
                    'data': serializer.data
               },
               status=status.HTTP_200_OK
          )
     else:
          send_log('error', 'Authorized user has failed to Top up E-Wallet with upload bukti pembayaran')
          return Response(
               {
                    'error' : serializer.errors
               },
               status=status.HTTP_400_BAD_REQUEST
          )

@require_http_methods(["POST"])
@api_view(['POST'])
def top_up_with_bank(request):
     if not 'HTTP_AUTHORIZATION' in request.META:
          send_log('error', 'Unauthorized user is accessing endpoint top up e-wallet with bank with no token')
          return Response(
               {
                    'error': 'No token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

     is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
     if not is_token_verified:
          send_log('error', 'Unauthorized user is accessing endpoint top up e-wallet with bank with invalid token')
          return Response(
               {
                    'error': 'Invalid token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

     data = request.data
     serializer = TopUpBankSerializer(data=data)
     if serializer.is_valid():
          serializer.save()
          send_log('info', 'Authorized user has Top up E-Wallet with bank and the transaction is being processed')
          return Response(
               {
                    'data': serializer.data
               },
               status=status.HTTP_200_OK
          )
     else:
          send_log('error', 'Authorized user has failed to Top up E-Wallet with bank')
          return Response(
               {
                    'error' : serializer.errors
               },
               status=status.HTTP_400_BAD_REQUEST
          )

          
@require_http_methods(["GET"])
@api_view(['GET'])
def get_ewallet(request, username):
     if not 'HTTP_AUTHORIZATION' in request.META:
          send_log('error', 'Unauthorized user is accessing endpoint getting E-Wallet with no token')
          return Response(
               {
                    'error': 'No token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

     is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
     if not is_token_verified:
          send_log('error', 'Unauthorized user is accessing endpoint getting E-Wallet with invalid token')
          return Response(
               {
                    'error': 'Invalid token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )
     if cache.get(username):
          send_log('info', 'Authorized user accessing endpoint to get E-Wallet, it comes from Cache')
          print('E Wallet come from Cache')
          ewallet = cache.get(username)
     else:
          send_log('info', 'Authorized user accessing endpoint to get E-Wallet, it comes from Database')
          print('E-Wallet come from database')
          ewallet = get_ewallet_instance(username)
          cache.set(username, ewallet, timeout=(60*5))     
     serializer = EWalletSerializer(ewallet)
     return Response(
          {
               'data': serializer.data
          },
          status=status.HTTP_200_OK
     )

@require_http_methods(["POST"])
@api_view(['POST'])
def pay_with_ewallet(request):
     if not 'HTTP_AUTHORIZATION' in request.META:
          send_log('error', 'Unauthorized user is accessing endpoint pay with E-Wallet with no token')
          return Response(
               {
                    'error': 'No token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

     is_token_verified = verify_token(request.META['HTTP_AUTHORIZATION'])
     if not is_token_verified:
          send_log('error', 'Unauthorized user is accessing endpoint pay with E-Wallet with invalid token')
          return Response(
               {
                    'error': 'Invalid token provided!'
               },
               status=status.HTTP_401_UNAUTHORIZED
          )

     data = request.data
     serializer = PembayaranDenganEWalletSerializer(data=data)
     if serializer.is_valid():
          serializer.save()
          ewallet = get_ewallet_instance(data['username'])
          if ewallet.saldo == 0 or ewallet.saldo - int(data['nominal']) < 0 :
               send_log('warn', 'Authorized user wanted to pay with E-Wallet but has not got enough balance')
               return Response(
                    {
                         'error' : 'saldo tidak cukup'
                    },
                    status=status.HTTP_400_BAD_REQUEST
               )
          else:   
               ewallet.saldo -= int(data['nominal'])
               ewallet.save()
               cache.set(ewallet.username, ewallet, timeout=(60*5))
               send_log('info', 'Authorized user has pay with E-Wallet and the transaction is finished')
          return Response(
               {
                    'data': serializer.data
               },
               status=status.HTTP_200_OK
          )
     else: 
          send_log('error', 'Authorized user has failed to pay with E-Wallet')
          return Response(
               {
                    'error' : serializer.errors
               },
               status=status.HTTP_400_BAD_REQUEST
          )


def get_ewallet_instance(dataUsername):
     ewallet = None
     try:
          ewallet = EWallet.objects.get(username=dataUsername)
     except EWallet.DoesNotExist:
          ewallet = EWallet.objects.create(
               username=dataUsername,
               saldo=0
          )
     return ewallet